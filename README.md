Venn diagrams for gplots
========================

This repository maintains and update and extension of the Venn diagramms of the R package gplots for 6 and 7 dimensions. It also provides an abstract graph representation that is unlimited.

![Ellipses and corresponding graph structure](vignettes/4dimEllipsesWithGraph.png)
*Ellipses and corresponding graph structure:* For 3 and more dimensions, no circular representation is possible. This package offers ellipsoidal representations for four and five dimensional Venn diagrams. This image features an overlay explaining the corresponding graph structure.

![Polyominoes and corresponding graph structure](vignettes/4dimPolyominoesWithGraph.png)
*Polyominoes and correpsonding graph structure:* This "squared layout" sees an increased number of intersections towards the center while moving horizontally or vertically. Neighbouring cells differ only by a single extra or removed set. Again, the corresponding graph representation is shown.

The vignette presents a vivid introduction to the use of Venn diagrams with this package.
